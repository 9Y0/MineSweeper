package me.VetBakSim.MineSweeper.util;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import me.VetBakSim.MineSweeper.model.Tile;

public class Images {

	public static final Image BOMB_IMAGE = new Image(
			Tile.class.getClassLoader().getResourceAsStream("assets/bomb.png"));
	public static final Image FLAG_IMAGE = new Image(
			Tile.class.getClassLoader().getResourceAsStream("assets/flag.png"));
	public static final Image WRONG_FLAG_IMAGE = new Image(
			Tile.class.getClassLoader().getResourceAsStream("assets/wrongflag.png"));

	public static ImageView getImage(Image image, int size) {
		ImageView imageView = new ImageView(image);

		imageView.setFitHeight(size);
		imageView.setFitWidth(size);

		return imageView;
	}

}
