package me.VetBakSim.MineSweeper.model;

public class TileStyle {

	private static final String formattedString = "-fx-text-fill: %s; -fx-font-size: %spt;";

	public static String getStyleSheet(int neighbourBombs, int fontSize) {
		switch (neighbourBombs) {
			case 1 :
				return String.format(formattedString, "#0000ff", fontSize);
			case 2 :
				return String.format(formattedString, "#00a000", fontSize);
			case 3 :
				return String.format(formattedString, "#ff0000", fontSize);
			case 4 :
				return String.format(formattedString, "#00007f", fontSize);
			case 5 :
				return String.format(formattedString, "#a00000", fontSize);
			case 6 :
				return String.format(formattedString, "#00ffff", fontSize);
			case 7 :
				return String.format(formattedString, "#a000a0", fontSize);
			case 8 :
				return String.format(formattedString, "#000000", fontSize);
			default :
				return String.format(formattedString, "#000000", fontSize);
		}
	}

}
