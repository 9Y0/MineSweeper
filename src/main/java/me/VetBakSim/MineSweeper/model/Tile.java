package me.VetBakSim.MineSweeper.model;

import java.util.Arrays;

import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseButton;
import me.VetBakSim.MineSweeper.controller.Game;
import me.VetBakSim.MineSweeper.util.Images;

public class Tile extends ToggleButton {

	private final int size;

	private final Game game;
	private final int x;
	private final int y;
	private TileType type;
	private int bombsCount;
	private boolean flagged;
	private boolean opened;

	public Tile(Game game, int x, int y, int tileSize) {
		this.size = tileSize;
		this.game = game;
		this.x = x;
		this.y = y;
		this.setMinSize(this.size, this.size);
		this.setMaxSize(this.size, this.size);
		this.setOnMouseClicked(click -> onMouseClick(click.getButton()));
		this.opened = false;
	}

	private void onMouseClick(MouseButton mouseButton) {
		if (game.isEnding()) {
			setSelected(!isSelected());
			return;
		}

		if (opened && !flagged) {
			setSelected(true);
			return;
		}

		opened = true;
		setSelected(true);

		if (mouseButton == MouseButton.PRIMARY) {
			if (!flagged)
				open();
		} else if (mouseButton == MouseButton.SECONDARY) {
			if (flagged) {
				game.setFlaggedTiles(game.getFlaggedTiles() - 1);
				flagged = false;
				opened = false;
				setSelected(false);
				setGraphic(null);
			} else {
				game.setFlaggedTiles(game.getFlaggedTiles() + 1);
				flagged = true;
				setGraphic(Images.getImage(Images.FLAG_IMAGE, size));
			}
		}
	}

	private void open() {
		if (type == TileType.BOMB) {
			setGraphic(Images.getImage(Images.BOMB_IMAGE, size));
			game.endGame(false);
		} else {
			if (bombsCount == 0) {
				Arrays.stream(game.getNeighbours(this))
						.forEach(neighbour -> neighbour.onMouseClick(MouseButton.PRIMARY));
			} else {
				Arrays.stream(game.getNeighbours(this))
						.filter(neighbour -> neighbour.getBombsCount() == 0 && neighbour.getType() == TileType.TILE)
						.forEach(neighbour -> neighbour.onMouseClick(MouseButton.PRIMARY));
				setText(bombsCount + "");
			}

			// TODO: Make a less RAM using check for checking if player won
			if (game.getTiles().length == Arrays.stream(game.getTiles()).filter(Tile::isOpened).count()) {
				game.endGame(true);
			}
		}
	}

	public void show() {
		setSelected(true);
		if (type == TileType.BOMB)
			setGraphic(Images.getImage(Images.BOMB_IMAGE, size));
		else if (bombsCount != 0) {
			setText(bombsCount + "");
		}
	}

	public boolean isWrongFlagged() {
		return flagged && type == TileType.TILE;
	}

	public TileType getType() {
		return type;
	}

	public void setType(TileType type) {
		this.type = type;
	}

	private int getBombsCount() {
		return bombsCount;
	}

	public void setBombsCount(int bombsCount) {
		this.bombsCount = bombsCount;
		if (bombsCount > 0)
			this.setStyle(TileStyle.getStyleSheet(bombsCount, size / 3));
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean isOpened() {
		return opened;
	}

}
