package me.VetBakSim.MineSweeper;

import javafx.application.Application;
import me.VetBakSim.MineSweeper.view.MineSweeperApplication;

public class Main {

	public static void main(String[] args) {
		Application.launch(MineSweeperApplication.class, args);
	}

}
