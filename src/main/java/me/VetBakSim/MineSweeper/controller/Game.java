package me.VetBakSim.MineSweeper.controller;

import java.util.*;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.Pair;
import me.VetBakSim.MineSweeper.model.Tile;
import me.VetBakSim.MineSweeper.model.TileType;
import me.VetBakSim.MineSweeper.util.Images;
import me.VetBakSim.MineSweeper.view.MineSweeperApplication;

public class Game {

	private final MineSweeperApplication mineSweeperApplication;

	private int tileSize;
	private int horizontalTiles;
	private int verticalTiles;
	private int bombsSize;

	private IntegerProperty flaggedTiles;

	private Tile[][] tiles;
	private BooleanProperty ending;

	public Game(MineSweeperApplication mineSweeperApplication) {
		this.mineSweeperApplication = mineSweeperApplication;
		this.createPlayField();
	}

	public void createPlayField() {
		this.ending = new SimpleBooleanProperty(false);

		Dialog<Pair<Integer, Integer>> dialog = new Dialog<>();
		dialog.setTitle("How big do you want the play field?");
		dialog.setHeaderText(null);

		ButtonType startButtonType = new ButtonType("Start", ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(startButtonType);

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField widthField = new TextField();
		widthField.setPromptText("Width");
		TextField heightField = new TextField();
		heightField.setPromptText("Height");

		grid.add(new Label("Width:"), 0, 0);
		grid.add(widthField, 1, 0);
		grid.add(new Label("Height:"), 0, 1);
		grid.add(heightField, 1, 1);

		Node startButton = dialog.getDialogPane().lookupButton(startButtonType);
		startButton.setDisable(true);

		widthField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.matches("\\d*")) {
				widthField.setText(newValue);
			} else {
				widthField.setText(oldValue);
			}

			startButton.setDisable(newValue.trim().isEmpty() || heightField.getText().isEmpty());
		});
		heightField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.matches("\\d*")) {
				heightField.setText(newValue);
			} else {
				heightField.setText(oldValue);
			}

			startButton.setDisable(newValue.trim().isEmpty() || widthField.getText().isEmpty());
		});

		dialog.getDialogPane().setContent(grid);

		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == startButtonType) {
				return new Pair<>(Integer.parseInt(widthField.getText()), Integer.parseInt(heightField.getText()));
			}
			return null;
		});

		IntegerProperty width = new SimpleIntegerProperty(10);
		IntegerProperty height = new SimpleIntegerProperty(10);

		dialog.showAndWait().ifPresent(widthAndHeight -> {
			width.set(widthAndHeight.getKey());
			height.set(widthAndHeight.getValue());
		});

		this.flaggedTiles = this.flaggedTiles == null ? new SimpleIntegerProperty(0) : this.flaggedTiles;

		this.horizontalTiles = width.get();
		this.verticalTiles = height.get();

		this.bombsSize = (int) Math.round(this.horizontalTiles * this.verticalTiles / 6.4);

		this.tileSize = 900 / Math.max(this.horizontalTiles, this.verticalTiles);
		this.tiles = new Tile[this.horizontalTiles][this.verticalTiles];
		for (int x = 0; x < this.horizontalTiles; x++) {
			for (int y = 0; y < this.verticalTiles; y++) {
				tiles[x][y] = new Tile(this, x, y, this.tileSize);
			}
		}

		createBombs();
	}

	private void createBombs() {
		List<Tile> randomTiles = Arrays.asList(getTiles());
		Collections.shuffle(randomTiles);
		for (int i = 0; i < bombsSize; i++) {
			randomTiles.get(i).setType(TileType.BOMB);
		}

		for (Tile tile : getTiles()) {
			if (tile.getType() == null) {
				tile.setType(TileType.TILE);
				tile.setBombsCount((int) Arrays.stream(getNeighbours(tile))
						.filter(neighbour -> neighbour.getType() == TileType.BOMB).count());
			}
		}
	}

	public void endGame(boolean playerWon) {
		ending.set(true);

		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setHeaderText(null);
		((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(Images.BOMB_IMAGE);
		ButtonType restartButton = new ButtonType("Restart");
		ButtonType closeButton = new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(restartButton, closeButton);
		alert.setGraphic(new ImageView(Images.BOMB_IMAGE));

		if (playerWon) {
			alert.setTitle("You won!");
			alert.setContentText("You won the game!");

			alert.showAndWait().ifPresent(buttonType -> {
				if (buttonType == restartButton) {
					mineSweeperApplication.restart();
				} else if (buttonType == closeButton) {
					System.exit(0);
				}
			});
		} else {
			alert.setTitle("You lost!");
			alert.setContentText("You clicked on a bomb!");

			Tile[] openTiles = Arrays.stream(getTiles()).filter(tile -> !tile.isOpened() || tile.isWrongFlagged())
					.toArray(Tile[]::new);

			IntegerProperty tileIndex = new SimpleIntegerProperty(0);
			Timeline timeline = new Timeline(new KeyFrame(Duration.millis(2000 / openTiles.length + 1), tick -> {
				Tile tile = openTiles[tileIndex.get()];

				if (tile.isWrongFlagged())
					tile.setGraphic(new ImageView(Images.WRONG_FLAG_IMAGE));
				else
					tile.show();

				tileIndex.set(tileIndex.get() + 1);
			}));
			timeline.setCycleCount(openTiles.length);
			timeline.play();
			timeline.setOnFinished(event -> Platform.runLater(() -> alert.showAndWait().ifPresent(buttonType -> {
				if (buttonType == restartButton) {
					mineSweeperApplication.restart();
				} else if (buttonType == closeButton) {
					System.exit(0);
				}
			})));
		}
	}

	public Tile[] getNeighbours(Tile tile) {
		Set<Tile> neighbours = new HashSet<>();

		for (int x = tile.getX() - 1; x <= tile.getX() + 1; x++) {
			for (int y = tile.getY() - 1; y <= tile.getY() + 1; y++) {
				if (x >= 0 && x < horizontalTiles && y >= 0 && y < verticalTiles) {
					Tile neighbour = getTile(x, y);
					if (!neighbour.equals(tile)) {
						neighbours.add(neighbour);
					}
				}
			}
		}

		return neighbours.toArray(new Tile[neighbours.size()]);
	}

	public Tile[] getTiles() {
		Set<Tile> tiles = new HashSet<>();
		Arrays.stream(this.tiles).forEach(tileArray -> Collections.addAll(tiles, tileArray));
		return tiles.toArray(new Tile[tiles.size()]);
	}

	private Tile getTile(int x, int y) {
		return tiles[x][y];
	}

	public IntegerProperty flaggedTilesProperty() {
		return flaggedTiles;
	}

	public int getFlaggedTiles() {
		return flaggedTiles.get();
	}

	public void setFlaggedTiles(int flaggedTiles) {
		this.flaggedTiles.set(flaggedTiles);
	}

	public int getBombsSize() {
		return bombsSize;
	}

	public int getTileSize() {
		return tileSize;
	}

	public int getHorizontalTiles() {
		return horizontalTiles;
	}

	public int getVerticalTiles() {
		return verticalTiles;
	}

	public boolean isEnding() {
		return ending.get();
	}

	public BooleanProperty endingProperty() {
		return ending;
	}
}
