package me.VetBakSim.MineSweeper.view;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.util.Duration;
import me.VetBakSim.MineSweeper.controller.Game;

public class TopPane extends HBox {

	private static final String TIME = "Time: %ss";
	private static final String FLAGS_USED = "Flags used: %s/%s";
	private final Game game;

	private Timeline timeRunnable;
	private final Label timeLabel;

	private final Label flagsUsedLabel;

	// TODO: Make some stats over here
	public TopPane(Game game) {
		this.setAlignment(Pos.CENTER);

		this.game = game;
		this.timeLabel = new Label();
		this.timeLabel.setMinWidth(200);
		this.timeLabel.setFont(Font.font(16));
		this.timeLabel.setText(String.format(TIME, 0));
		IntegerProperty i = new SimpleIntegerProperty(1);
		this.timeRunnable = new Timeline();
		this.timeRunnable.getKeyFrames().add(new KeyFrame(Duration.seconds(1), tick -> {
			this.timeLabel.setText(String.format(TIME, i.get()));
			i.set(i.get() + 1);
		}));
		this.timeRunnable.setCycleCount(Timeline.INDEFINITE);
		this.timeRunnable.play();
		this.game.endingProperty().addListener(observable -> {
			if (this.game.isEnding())
				this.timeRunnable.stop();
		});

		this.flagsUsedLabel = new Label();
		this.flagsUsedLabel.setMinWidth(200);
		this.flagsUsedLabel.setFont(Font.font(16));
		this.flagsUsedLabel.setText(String.format(FLAGS_USED, 0, game.getBombsSize()));
		// TODO: Fix that this updates when new gme starts
		this.game.flaggedTilesProperty().addListener(observable -> this.flagsUsedLabel
				.setText(String.format(FLAGS_USED, game.getFlaggedTiles(), game.getBombsSize())));

		getChildren().addAll(this.timeLabel, this.flagsUsedLabel);
	}

	public void reset() {
		this.timeRunnable.stop();
		timeLabel.setText(String.format(TIME, 0));
		IntegerProperty i = new SimpleIntegerProperty(1);
		this.timeRunnable = new Timeline();
		timeRunnable.getKeyFrames().add(new KeyFrame(Duration.seconds(1), tick -> {
			timeLabel.setText(String.format(TIME, i.get()));
			i.set(i.get() + 1);
		}));
		timeRunnable.setCycleCount(Timeline.INDEFINITE);
		timeRunnable.play();
	}

}
