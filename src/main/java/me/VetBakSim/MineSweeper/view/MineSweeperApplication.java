package me.VetBakSim.MineSweeper.view;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import me.VetBakSim.MineSweeper.controller.Game;
import me.VetBakSim.MineSweeper.util.Images;

public class MineSweeperApplication extends Application {

	private Game game;
	private TopPane topPane;
	private GameView gameView;
	private Stage primaryStage;

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;

		BorderPane pane = new BorderPane();

		this.game = new Game(this);

		this.topPane = new TopPane(game);
		this.gameView = new GameView(game);

		pane.setTop(topPane);
		pane.setCenter(gameView);

		Scene scene = new Scene(pane);
		primaryStage.setScene(scene);
		primaryStage.getIcons().add(Images.BOMB_IMAGE);
		primaryStage.setTitle("MineSweeper");
		primaryStage.setResizable(false);

		primaryStage.show();
	}

	public void restart() {
		primaryStage.hide();

		game.createPlayField();
		gameView.update();
		topPane.reset();

		primaryStage.show();

		System.out.println(gameView.getWidth());
	}

}
