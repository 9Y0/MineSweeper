package me.VetBakSim.MineSweeper.view;

import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import me.VetBakSim.MineSweeper.controller.Game;
import me.VetBakSim.MineSweeper.model.Tile;

public class GameView extends GridPane {

	private final Game game;

	public GameView(Game game) {
		this.game = game;

		int gapSize = game.getTileSize() / 20;
		this.setPadding(new Insets(gapSize, gapSize, gapSize, gapSize));
		this.setHgap(gapSize);
		this.setVgap(gapSize);

		this.update();
	}

	public void update() {
		getChildren().removeIf(node -> true);

		int gapSize = game.getTileSize() / 20;
		setPadding(new Insets(gapSize, gapSize, gapSize, gapSize));
		setHgap(gapSize);
		setVgap(gapSize);

		setPrefSize(game.getHorizontalTiles() * (game.getTileSize() + getVgap()),
				game.getVerticalTiles() * (game.getTileSize() + getHgap()));

		for (Tile tile : game.getTiles()) {
			GridPane.setConstraints(tile, tile.getX(), tile.getY());
			getChildren().add(tile);
		}
	}

}
